#include "comms_receive_via_socket.h"
#include <sys/socket.h>

namespace comms {
  receive_comms_socket::receive_comms_socket(std::string t_ip, int t_port){
    this->ip = t_ip;
    this->port = t_port;
  }

  receive_comms_socket::~receive_comms_socket(){
    this->disconnect_from_comms();
  }

  void receive_comms_socket::connect_to_comms() {
    int socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    int opt = 1;

    if (socket_fd < 0) {
      std::cout << "ERROR opening socket" << std::endl;
      exit(1);
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(this->port);


    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0) {
      std::cout << "setsockopt(SO_REUSEADDR) failed" << std::endl;
      exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(this->port);

    if (bind(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
      std::cout << "ERROR on binding" << std::endl;
      exit(1);
    }

    this->socket_fd = socket_fd;
  }

  void receive_comms_socket::disconnect_from_comms() {
    close(this->socket_fd);
  }

  fira_message::Frame receive_comms_socket::receive() {
    fira_message::Frame frame;
    fira_message::sim_to_ref::Environment environment;
    char buffered_message[1024] = {0};

    recv(this->socket_fd, &buffered_message, sizeof(buffered_message), 0);
    environment = this->_desserialize_message(buffered_message);
    frame = environment.frame();
    return frame;
  }

  fira_message::sim_to_ref::Environment receive_comms_socket::_desserialize_message(std::string t_message) {
    fira_message::sim_to_ref::Environment environment;
    environment.ParseFromString(t_message);
    return environment;
  }
}
