#include <string>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include "receive_comms.h"
#include "comms/packet.pb.h"

namespace comms {
  class receive_comms_socket : public receive_comms {
    public:
      receive_comms_socket(std::string t_ip, int t_port);
      ~receive_comms_socket();
      void connect_to_comms();
      void disconnect_from_comms();
      fira_message::Frame receive();
    private:
      int socket_fd;
      int port;
      std::string ip;
      fira_message::sim_to_ref::Environment _desserialize_message(std::string message);
  };
};
