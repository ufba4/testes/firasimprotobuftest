all:
	- make protobuf && make compile

protobuf:
	- cd comms && protoc --cpp_out=. ./*.proto

clean-protobuf:
	- rm comms/*.pb.*

compile:
	- g++ -std=c++20 -I/usr/local/include -pthread -c *.cpp comms/*.cc
	- g++ -o main *.o -L/usr/local/lib -lprotobuf -lpthread

clean:
	- make clean-protobuf
	- rm -rf *.o *.gch comms/firasim/*.o comms/firasim/*.gch main

