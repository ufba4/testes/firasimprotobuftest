#include <string>
#include "comms/packet.pb.h"

namespace comms {
  class send_comms {
    public:
      virtual void connect_to_comms();
      virtual void send_data(fira_message::sim_to_ref::Packet packet);
      virtual void disconnect_to_comms();
      std::string _serialize(fira_message::sim_to_ref::Packet packet);
  };
};
