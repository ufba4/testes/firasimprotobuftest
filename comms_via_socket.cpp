#include "comms_via_socket.h"

namespace comms {
  send_comms_socket::send_comms_socket(std::string t_ip, int t_port) {
    this->ip = t_ip;
    this->port = t_port;
  }

  send_comms_socket::~send_comms_socket() {
    this->disconnect_to_comms();
  }

  void send_comms_socket::connect_to_comms() {
    int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_fd < 0) {
      std::cout << "ERROR opening socket" << std::endl;
      exit(1);
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(this->port);
    if (inet_pton(AF_INET, this->ip.c_str(), &server_addr.sin_addr) <= 0) {
      std::cout << "ERROR converting IP address" << std::endl;
      exit(1);
    }

    if (connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
      std::cout << "ERROR connecting" << std::endl;
      exit(1);
    }

    this->socket_fd = socket_fd;
  }

  void send_comms_socket::disconnect_to_comms() {
    close(this->socket_fd);
  }

  void send_comms_socket::send_data(fira_message::sim_to_ref::Packet packet) {
    std::string serialized_packet = this->_serialize(packet);
    if (send(this->socket_fd, serialized_packet.c_str(), serialized_packet.length(), 0) < 0) {
      std::cout << "ERROR sending packet" << std::endl;
      exit(1);
    }
  }

  std::string send_comms_socket::_serialize(fira_message::sim_to_ref::Packet packet) {
    std::string serialized_packet;
    packet.SerializeToString(&serialized_packet);
    return serialized_packet;
  }
}
