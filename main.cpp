#include "comms/command.pb.h"
#include "comms/common.pb.h"
#include "comms/packet.pb.h"
#include "comms_via_socket.h"
#include "comms_receive_via_socket.h"
#define PORT_YELLOW 20011
#define PORT_LISTEN 10002

int main() {
  std::cout << "Make yellow robot 1" << std::endl;
  std::cout << "Port: " << PORT_YELLOW << std::endl;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  comms::send_comms_socket c = comms::send_comms_socket("127.0.0.1", PORT_YELLOW);
  comms::receive_comms_socket r = comms::receive_comms_socket("127.0.0.1", PORT_LISTEN);


  // Create robot protobuf message
  fira_message::sim_to_ref::Commands commands = fira_message::sim_to_ref::Commands();
  fira_message::sim_to_ref::Command *command_ptr;
  command_ptr = commands.add_robot_commands();
  command_ptr->set_id(0);
  command_ptr->set_yellowteam(true);
  command_ptr->set_wheel_left(10);
  command_ptr->set_wheel_right(10);
  fira_message::sim_to_ref::Packet packet = fira_message::sim_to_ref::Packet();
  packet.set_allocated_cmd(&commands);

  c.connect_to_comms();
  c.send_data(packet);

  // Delocate CMD
  packet.release_cmd();
  c.disconnect_to_comms();

  std::cout << "Getting data" << std::endl;
  r.connect_to_comms();
  for (int i = 0; i < 10000; i++) {
    fira_message::Frame frame = r.receive();
    std::cout << "Ball position: " << frame.ball().x() << ", " << frame.ball().y() << std::endl;
  }
  std::cout << "Received data" << std::endl;
  r.disconnect_from_comms();

  // Send message to server for 1 second
  std::cout << "Done" << std::endl;
  return 0;
}
