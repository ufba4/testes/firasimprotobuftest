#include <string>
#include <iostream>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string>
#include "comms/packet.pb.h"
#include "send_comms.h"

namespace comms {
  class send_comms_socket : public send_comms {
    public:
      send_comms_socket(std::string t_ip, int t_port);
      ~send_comms_socket();
      void connect_to_comms();
      void send_data(fira_message::sim_to_ref::Packet packet);
      void disconnect_to_comms();
      std::string _serialize(fira_message::sim_to_ref::Packet packet);
    private:
      std::string ip;
      int port;
      int socket_fd;
  };
};

