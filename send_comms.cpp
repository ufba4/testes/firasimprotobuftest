#include "send_comms.h"

namespace comms {
  std::string send_comms::_serialize(fira_message::sim_to_ref::Packet packet) {
    std::string serialized_packet;
    packet.SerializeToString(&serialized_packet);
    return serialized_packet;
  }

  void send_comms::connect_to_comms() {}
  void send_comms::send_data(fira_message::sim_to_ref::Packet packet) {}
  void send_comms::disconnect_to_comms() {}
};
