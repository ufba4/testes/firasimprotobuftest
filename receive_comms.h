#include "comms/common.pb.h"

namespace comms {
  class receive_comms {
    public:
      virtual void connect_to_comms();
      virtual void disconnect_from_comms();
      virtual fira_message::Frame receive();
  };
};
